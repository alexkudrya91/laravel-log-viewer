![logo](images/Logo.png)

[![Total Downloads](https://poser.pugx.org/alex-kudrya/laravel-log-viewer/downloads)](//packagist.org/packages/alex-kudrya/laravel-log-viewer)
[![Version](https://poser.pugx.org/alex-kudrya/laravel-log-viewer/version)](//packagist.org/packages/alex-kudrya/laravel-log-viewer)
[![License](https://poser.pugx.org/alex-kudrya/laravel-log-viewer/license)](//packagist.org/packages/alex-kudrya/laravel-log-viewer)
[![PHP Version Require](http://poser.pugx.org/alex-kudrya/laravel-log-viewer/require/php)](https://packagist.org/packages/alex-kudrya/laravel-log-viewer)

<h2>Laravel package for logs exploring.</h2>

![preview](images/preview.png)

<h2>Installation</h2>

```shell
composer require alex-kudrya/laravel-log-viewer
```

In `app.php` add to providers array `\AlexKudrya\LaravelLogViewer\Providers\LogViewerServiceProvider::class`

```php
// config/app.php

'providers' => [
    ...
    \AlexKudrya\LaravelLogViewer\Providers\LogViewerServiceProvider::class,
    ...
 ]
```

Publish package config and assets to your app

```shell
php artisan vendor:publish --provider=AlexKudrya\LaravelLogViewer\Providers\LogViewerServiceProvider
```


Strongly recommended to disallow indexing Log Viewer pages for search engines in `robots.txt`:

```txt
User-agent: *

Allow: /
Disallow: /log-viewer
```

**Congratulations! Log Viewer now allow by this URI - `https://[your-host]/log-viewer`**

<h2>Configuration</h2>

All configs for Log Viewer located on `config/log_viewer.php`

**`route_prefix`** - URI for Log Viewer page. By defualt it is equal `log-viewer`, means that URL for Log Viewer will be `https://{{your_host}}/log-viewer`.

**`app_title`** - Title for your Log Viewer page, located in `<head/>` -> `<title/>` tag.

**`middleware`** - middleware(s) for Log Viewer route, for example `'auth'` or some else `middleware` class, or **array** of them.

**`max_display_records`** - Maximum number of records which can be displayed. Some logs can contain hundreds of thousands of lines, and it is too many to be displayed in one time. Pagination for log doesn't make sense, so we need to set this limit. By default is **10000**

<h2>Author</h2>

**Alexander Kudria [alexkudrya91@gmail.com](mailto:alexkudrya91@gmail.com)**
