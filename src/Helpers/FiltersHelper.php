<?php

namespace AlexKudrya\LaravelLogViewer\Helpers;

use AlexKudrya\LaravelLogViewer\Dto\FiltersDto;
use Illuminate\Support\Facades\Cookie;

class FiltersHelper
{
    public function prepareFilters(array &$filters, array $settings): void
    {
        if (array_key_exists('lvl', $filters)) {
            $filters['lvl'] = explode(',', $filters['lvl']);
        }

        if (array_key_exists('env', $filters)) {
            $filters['env'] = explode(',', $filters['env']);
        }

        $filters['hide_date'] = $settings['hide_date'] ?? false;
        $filters['hide_env'] = $settings['hide_env'] ?? false;
        $filters['hide_lvl'] = $settings['hide_lvl'] ?? false;
        $filters['locale'] = $settings['locale'] ?? 'en';
    }

    public function filtersToDto(array $filters): FiltersDto
    {
        $dto = new FiltersDto();

        if (array_key_exists('search', $filters)) {
            $dto->search(explode(' ', $filters['search']));
        }

        if (array_key_exists('lvl', $filters)) {
            $dto->levels($filters['lvl']);
        }

        if (array_key_exists('env', $filters)) {
            $dto->environments($filters['env']);
        }

        if (array_key_exists('day', $filters)) {
            $dto->date($filters['day']);
        }

        if (array_key_exists('tmin', $filters)) {
            $dto->timeFrom($filters['tmin'].':00');
        }

        if (array_key_exists('tmax', $filters)) {
            $dto->timeTo($filters['tmax'].':59');
        }

        if (array_key_exists('hide_date', $filters) && $filters['hide_date']) {
            $dto->hideDates();
        }

        if (array_key_exists('hide_env', $filters) && $filters['hide_env']) {
            $dto->hideEnvironments();
        }

        if (array_key_exists('hide_lvl', $filters) && $filters['hide_lvl']) {
            $dto->hideLevels();
        }

        if (array_key_exists('locale', $filters) && $filters['locale']) {
            $dto->locale($filters['locale']);
        }

        return $dto;
    }

    public function getSettings(): array
    {
        return [
            'hide_date' => Cookie::get('hide_date'),
            'hide_env' => Cookie::get('hide_env'),
            'hide_lvl' => Cookie::get('hide_lvl'),
            'locale' => Cookie::get('log_viewer_locale'),
        ];
    }
}
