<?php

return [
    'btn' => [
        'apply' => 'Apply',
        'reset' => 'Reset',
        'default' => 'Default',
    ],

    'menu' => [
        'filters' => 'Filters',
        'search' => 'Search',
        'download' => 'Download',
        'settings' => 'Settings',
    ],

    'settings' => [
        'hide_dates' => 'Hide Dates if they are the same',
        'hide_env' => 'Hide Environments if they are the same',
        'hide_lvl' => 'Hide Levels if they are the same',
        'lang' => 'Language',
    ],

    'filters' => [
        'date' => 'Date',
        'time' => 'Time',
        'from' => 'From',
        'to' => 'To',
        'env' => 'Environment',
        'lvl' => 'Level',
    ],

    'type_request' => 'Type your request',

    'select_file' => 'Select log file to explore.',
    'no_matches' => 'There are no records in file that match your search criteria.',
    'file_empty' => 'File is empty.',

    'theme' => [
        'system' => 'System',
        'light' => 'Light',
        'dark' => 'Dark',
    ],

    'stacktrace' => 'Stacktrace',
    'context' => 'Context',
    'default_filters' => 'Default all filters',
    'collapse' => 'Collapse',
];
