<?php

namespace AlexKudrya\LaravelLogViewer\Traits;

use Exception;

trait MagicMethods
{
    public static function __callStatic(string $name, array $arguments): self
    {
        $obj = new static();

        $method = 'set' . ucfirst($name);

        if (method_exists($obj, $method)) {
            $obj->$method($arguments[0]);

            return $obj;
        }

        throw new Exception('Invalid ' . __CLASS__ . ' method name ' . $method);
    }

    public function __call(string $name, array $arguments)
    {
        if (!$arguments) {
            $method = 'get' . ucfirst($name);

            if (!method_exists($this, $method)) {
                $method = 'is' . ucfirst($name);
            }

            if (!method_exists($this, $method)) {
                $method = 'has' . ucfirst($name);
            }

            if (method_exists($this, $method)) {
                return $this->$method();
            }
        } else {
            $method = 'set' . ucfirst($name);

            if (method_exists($this, $method)) {
                return $this->$method($arguments[0]);
            }
        }

        throw new Exception('Invalid ' . __CLASS__ . ' method name ' . $method);
    }
}
