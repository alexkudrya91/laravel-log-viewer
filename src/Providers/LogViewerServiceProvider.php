<?php

namespace AlexKudrya\LaravelLogViewer\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Route;

class LogViewerServiceProvider extends ServiceProvider
{
    public function boot(): void
    {
        if ($this->app->runningInConsole()) {
            $this->publishes([
                __DIR__ . '/../config/log_viewer.php' => config_path('log_viewer.php')
            ], 'config');

            $this->publishes([
                __DIR__.'/../resources/views/pages' => $this->app->resourcePath('views/vendor/log_viewer'),
                __DIR__.'/../resources/views/components' => $this->app->resourcePath('views/components/vendor/log_viewer'),
                __DIR__.'/../resources/css' => $this->app->publicPath('css/log_viewer'),
                __DIR__.'/../resources/js' => $this->app->publicPath('js/log_viewer'),
                __DIR__.'/../resources/favicon' => $this->app->publicPath('favicon'),
                __DIR__.'/../resources/fonts' => $this->app->publicPath('fonts/log_viewer'),
                __DIR__.'/../lang/en.php' => $this->app->langPath('en/log_viewer.php'),
                __DIR__.'/../lang/ru.php' => $this->app->langPath('ru/log_viewer.php'),
                __DIR__.'/../lang/ua.php' => $this->app->langPath('ua/log_viewer.php'),
            ], 'laravel-assets');
        }

        $middleware = ['web'];
        if (config('log_viewer.middleware')) {
            if (is_array(config('log_viewer.middleware'))) {
                $middleware = array_merge($middleware, config('log_viewer.middleware'));
            } else {
                $middleware[] = config('log_viewer.middleware');
            }
            $middleware = array_unique($middleware);
        }

        Route::middleware($middleware)
            ->prefix(config('log_viewer.route_prefix'))
            ->group(function () {
                $this->loadRoutesFrom(__DIR__ . '/../routes/web.php');
            });

        app('config')->set('logging.channels.log_viewer', [
            'driver' => 'daily',
            'path' => storage_path('logs/log_viewer.log'),
            'level' => env('LOG_LEVEL', 'debug'),
            'days' => 14,
            'replace_placeholders' => true,
        ]);
    }

}
