<?php

use Illuminate\Support\Facades\Route;

Route::get('/', [\AlexKudrya\LaravelLogViewer\Http\IndexController::class, 'index'])
    ->name('log_viewer.index');

Route::get('/{file}', [\AlexKudrya\LaravelLogViewer\Http\IndexController::class, 'download'])
    ->name('log_viewer.download');

Route::post('/settings', [\AlexKudrya\LaravelLogViewer\Http\IndexController::class, 'saveSettings'])
    ->name('log_viewer.settings');
