$( document ).ready(function() {
    const themeKey = "logViewerTheme"
    const themeCacheKey = "log_viewer_dark_theme"

    $('.theme-switcher').on('click', function () {
        let activeSwitcher = $('.switcher-btn:visible')

        activeSwitcher.hide()

        if (activeSwitcher.hasClass('switcher-auto')) {
            $('.switcher-light').show()
            $('body').removeClass('dark-mode')
            localStorage.setItem(themeKey, 'light');
            setCookie(themeCacheKey, 'false', 365)
        }

        if (activeSwitcher.hasClass('switcher-light')) {
            $('.switcher-dark').show()
            $('body').addClass('dark-mode')
            localStorage.setItem(themeKey, 'dark');
            setCookie(themeCacheKey, 'true', 365)
        }

        if (activeSwitcher.hasClass('switcher-dark')) {
            $('.switcher-auto').show()
            let systemTheme = window.matchMedia("(prefers-color-scheme: dark)").matches ? 'dark' : 'light';
            if (systemTheme === 'dark') {
                $('body').addClass('dark-mode')
                setCookie(themeCacheKey, 'true', 365)
            } else {
                $('body').removeClass('dark-mode')
                setCookie(themeCacheKey, 'false', 365)
            }

            localStorage.setItem(themeKey, 'auto');
        }
    })

    let checkTheme = function () {
        let theme = localStorage.getItem(themeKey);

        if (theme === null || theme === 'auto') {
            let systemTheme = window.matchMedia("(prefers-color-scheme: dark)").matches ? 'dark' : 'light';
            if (systemTheme === 'dark') {
                $('body').addClass('dark-mode')
                setCookie(themeCacheKey, 'true', 365)
            }
        } else if (theme === 'dark') {
            $('body').addClass('dark-mode')
            $('.switcher-btn:visible').hide()
            $('.switcher-dark').show()
        } else if (theme === 'light') {
            $('.switcher-btn:visible').hide()
            $('.switcher-light').show()
        }
    }

    function setCookie(cname, cvalue, exdays) {
        const d = new Date();
        d.setTime(d.getTime() + (exdays*24*60*60*1000));
        let expires = "expires="+ d.toUTCString();
        document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
    }

    checkTheme();
});
