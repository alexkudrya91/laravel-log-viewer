$( document ).ready(function() {

    $('.log-record').on('click', function () {
        if ( $( this ).hasClass('active')) return

        if ($( this ).find('.log-raw-context').length
            || $( this ).find('.log-context').length
            || $( this ).find('.log-stacktrace').length) {
            $( this ).addClass('active')
            return
        }

        let date = $( this ).find('.log-date');
        let time = $( this ).find('.log-time');
        let env = $( this ).find('.log-env');
        let level = $( this ).find('.log-level');
        let message = $( this ).find('.log-message');

        let sum_width = date.width() + time.width() + env.width() + level.width() + message.width()
        let width = $( this ).width()

        if (width < sum_width) $( this ).addClass('active')
    })

    $('.log-record .collapse-bottom-btn').on('click', function () {
        let log = $( this ).closest('.log-record')
        hideLog(log)
    })

    $('.log-record .collapse-top-btn').on('click', function () {
        let log = $( this ).closest('.log-record')
        hideLog(log)
    })

    const hideLog = function (log) {
        setTimeout(function () {
            if (log.hasClass('active')) log.removeClass('active')
        }, 100)
    }

    $('#search-btn').on('click', function () {
        $('#searchModal').modal('toggle');
    })

    $('#settings-btn').on('click', function () {
        $('#settingsModal').modal('toggle');
    })

    $('#filters-btn').on('click', function () {
        $('#filtersModal').modal('toggle');
    })

    $('#filters-default-btn').on('click', function () {
        const url = new URL(window.location.href)

        url.searchParams.delete('lvl')
        url.searchParams.delete('env')
        url.searchParams.delete('tmin')
        url.searchParams.delete('tmax')
        url.searchParams.delete('day')

        goToUrl(url.toString())
    })

    $('#default-all-filters').on('click', function () {
        const url = new URL(window.location.href)

        url.searchParams.delete('lvl')
        url.searchParams.delete('env')
        url.searchParams.delete('tmin')
        url.searchParams.delete('tmax')
        url.searchParams.delete('day')
        url.searchParams.delete('search')

        goToUrl(url.toString())
    })

    $('#settingsModal').on('hidden.bs.modal', function () {
        $('#settings-form').trigger("reset");
    })

    $('#filtersModal').on('hidden.bs.modal', function () {
        $('#filters-form').trigger("reset");
    })

    $('#searchModal').on('shown.bs.modal', function () {
        const input = $('#search-input');
        input.trigger('focus')

        // Move caret to end of the input
        let val = input.val();
        input.val('');
        input.val(val);
    })

    $('#search-input').keypress(function (e) {
        if (e.which === 13) {
            $('#search-form').submit();
            return false;
        }
    });

    $('#filters-form').on('submit', function (e) {
        e.preventDefault();

        let url = new URL(window.location.href)

        applyLevelFilter(url)
        applyEnvironmentFilter(url)
        applyTimeFilter(url)
        applyDateFilter(url)

        goToUrl(url.toString())
    })

    $('#search-form').on('submit', function (e) {
        e.preventDefault();

        const url = new URL(window.location.href)

        const query = $('#search-input').val();
        const searchKey = 'search';

        if (query.length > 0) {
            url.searchParams.set(searchKey, query)
        } else {
            url.searchParams.delete(searchKey)
        }

        goToUrl(url.toString())
    })

    const goToUrl = function (url) {
        window.location.href = url
    }

    const applyLevelFilter = (url) => {
        const levelsKey = 'lvl'
        const levels = [];
        let hasExceptedLevels = false;

        $('.level-list .level-input').each(function( index ) {
            if (this.checked) {
                levels.push($( this ).data('name'));
            } else {
                hasExceptedLevels = true;
            }
        });

        if (hasExceptedLevels && levels.length) {
            url.searchParams.set(levelsKey, levels.join(','))
        } else {
            url.searchParams.delete(levelsKey)
        }
    }

    const applyEnvironmentFilter = (url) => {
        const environmentsKey = 'env'
        const environments = [];
        let hasExceptedEnvironments = false;

        $('.env-list .env-input').each(function( index ) {
            if (this.checked) {
                environments.push($( this ).data('name'));
            } else {
                hasExceptedEnvironments = true;
            }
        });

        if (hasExceptedEnvironments && environments.length) {
            url.searchParams.set(environmentsKey, environments.join(','))
        } else {
            url.searchParams.delete(environmentsKey)
        }
    }

    const applyTimeFilter = (url) => {
        const filterFrom = $('#filter-time-from').val()
        const filterTo = $('#filter-time-to').val()

        const filterFromKey = 'tmin';
        const filterToKey = 'tmax';

        if (filterFrom.length) {
            url.searchParams.set(filterFromKey, filterFrom)
        } else {
            url.searchParams.delete(filterFromKey)
        }

        if (filterTo.length) {
            url.searchParams.set(filterToKey, filterTo)
        } else {
            url.searchParams.delete(filterToKey)
        }
    }

    const applyDateFilter = (url) => {
        const filterDate = $('#filter-date').val()

        const filterDateKey = 'day';

        if (filterDate.length) {
            url.searchParams.set(filterDateKey, filterDate)
        } else {
            url.searchParams.delete(filterDateKey)
        }
    }

    const highlightSearchWords = () => {
        const url = new URL(window.location.href)
        let search = url.searchParams.get('search')

        if (search) {
            let arr = search.split(" ");
            for (let i in arr) {
                let search_word = arr[i]
                $(".log-record").highlight(search_word);
            }
        }
    }

    highlightSearchWords();

    window.addEventListener("keydown",function (e) {
        if (e.keyCode === 114 || (e.ctrlKey && e.keyCode === 70)) {
            e.preventDefault();
            $('#searchModal').modal('show');
        }
    })
});
