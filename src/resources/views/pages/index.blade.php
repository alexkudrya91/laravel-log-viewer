@php
    /** @var \AlexKudrya\LaravelLogViewer\Dto\LogViewDto $dto */
    /** @var \AlexKudrya\LaravelLogViewer\Dto\LogRecordDto $log_record */
    /** @var \AlexKudrya\LaravelLogViewer\Dto\FiltersDto $filters */
@endphp
<x-vendor.log_viewer.layouts.layout>
    <main class="d-flex flex-nowrap">
        <x-vendor.log_viewer.sidebar :files="$dto->filesList()" :file="$file"/>
        <div class="container-fluid overflow-y-scroll hide-scrollbar position-relative">
            @if(is_null($file))
                <div class="content-center h-100">
                    <div class="row text-secondary text-center">
                        <p>{{ trans('log_viewer.select_file') }}</p>
                    </div>
                </div>
            @elseif($filters->isEmpty() && count($dto->logRecords()) === 0)
                <div class="content-center h-100">
                    <div class="row text-secondary text-center">
                        <p>{{ trans('log_viewer.file_empty') }}</p>
                    </div>
                </div>
            @elseif(!$filters->isEmpty() && count($dto->logRecords()) === 0)
                <div class="content-center h-100">
                    <div class="row text-secondary text-center">
                        <p>
                            {{ trans('log_viewer.no_matches') }}
                            <br>
                            <button type="button" class="btn mt-3" id="default-all-filters">{{ trans('log_viewer.default_filters') }}</button>
                        </p>
                    </div>
                </div>
            @else
                <div class="container position-relative">
                    <div class="row">
                        @foreach($dto->logRecords() as $log_record)
                            <x-vendor.log_viewer.log_record :log_record="$log_record"/>
                        @endforeach
                    </div>

                    <footer class="text-center my-3 text-secondary d-block text-center">&copy;
                        Laravel Log Viewer {{ \Illuminate\Support\Carbon::now()->format('Y') }}
                    </footer>
                </div>
            @endif
        </div>
    </main>

    <x-vendor.log_viewer.search_modal :filters="$filters"/>

    <x-vendor.log_viewer.filters_modal :filters="$filters" :dto="$dto"/>

    <x-vendor.log_viewer.settings_modal :filters="$filters"/>

</x-vendor.log_viewer.layouts.layout>
