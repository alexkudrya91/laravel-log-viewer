@php
    /** @var \AlexKudrya\LaravelLogViewer\Dto\LogViewDto $dto */
    /** @var \AlexKudrya\LaravelLogViewer\Dto\FiltersDto $filters */
    $dto = $attributes['dto'];
    $filters = $attributes['filters'];
@endphp
<div class="modal fade" id="filtersModal" tabindex="-1" aria-labelledby="filtersModalLabel" aria-modal="true">
    <div class="modal-dialog modal-dialog-centered modal-lg">
        <div class="modal-content">
            <div class="modal-header pb-0 px-4 border-bottom-0">
                <h6 class="modal-title fs-6" id="searchModalLabel">{{ trans('log_viewer.menu.filters') }}...</h6>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close" style="font-size: 12px;"></button>
            </div>
            <div class="modal-body pt-2">
                <form id="filters-form">
                    <table class="table">
                        <tr>
                            <td>{{ trans('log_viewer.filters.date') }}:</td>
                            <td>
                                <input class="form-control me-4 d-inline-block"
                                       id="filter-date"
                                       type="date"
                                       lang="{{ App::getLocale() }}"
                                       @if($filters->date()) value="{{$filters->date()}}" @endif
                                       max="{{$dto->getMaxDate()}}"
                                       min="{{$dto->getMinDate()}}">

                            </td>
                        </tr>
                        <tr>
                            <td>{{ trans('log_viewer.filters.time') }}:</td>
                            <td>
                                {{ trans('log_viewer.filters.from') }}: <input class="form-control d-inline-block me-3"
                                                                               id="filter-time-from"
                                                                               type="time"
                                                                               @if($filters->timeFrom()) value="{{$filters->timeFrom()}}" @endif>
                                {{ trans('log_viewer.filters.to') }}: <input class="form-control d-inline-block"
                                                                             id="filter-time-to"
                                                                             type="time"
                                                                             @if($filters->timeTo()) value="{{$filters->timeTo()}}" @endif>
                            </td>
                        </tr>

                        <tr>
                            <td>{{ trans('log_viewer.filters.env') }}:</td>
                            <td>
                                <div class="env-list">
                                    @foreach($dto->environments() as $environment)
                                        <div class="check-block d-block">
                                            <input class="form-check-input mt-0 env-input"
                                                   data-name="{{$environment}}"
                                                   id="env-{{strtolower($environment)}}"
                                                   type="checkbox"
                                                   @if(in_array($environment, $filters->environments()) || empty($filters->environments()))
                                                       checked
                                                   @endif
                                                   aria-label="Display {{$environment}} environment">
                                            <label for="env-{{strtolower($environment)}}">{{$environment}}</label>
                                        </div>
                                    @endforeach
                                </div>
                            </td>
                        </tr>

                        <tr>
                            <td>{{ trans('log_viewer.filters.lvl') }}:</td>
                            <td>
                                <div class="level-list">
                                    @foreach($dto->levels() as $level)
                                        <div class="check-block d-block">
                                            <input class="form-check-input mt-0 level-input"
                                                   data-name="{{$level}}"
                                                   id="level-{{strtolower($level)}}"
                                                   type="checkbox"
                                                   @if(in_array($level, $filters->levels()) || empty($filters->levels()))
                                                       checked
                                                   @endif
                                                   aria-label="Display {{$level}} environment">
                                            <label for="level-{{strtolower($level)}}">{{$level}}</label>
                                        </div>
                                    @endforeach
                                </div>
                            </td>
                        </tr>
                    </table>
                    <div class="text-center">
                        <button class="btn" type="submit">{{ trans('log_viewer.btn.apply') }}</button>
                        <button class="btn" type="reset">{{ trans('log_viewer.btn.reset') }}</button>
                        <button class="btn" type="button" id="filters-default-btn">{{ trans('log_viewer.btn.default') }}</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
