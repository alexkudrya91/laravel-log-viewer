@php
    /** @var \AlexKudrya\LaravelLogViewer\Dto\FiltersDto $filters */
    $filters = $attributes['filters']
 @endphp

<div class="modal fade" id="searchModal" tabindex="-1" aria-labelledby="searchModalLabel" aria-modal="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header pb-0 px-4 border-bottom-0">
                <h6 class="modal-title fs-6" id="searchModalLabel">{{ trans('log_viewer.menu.search') }}...</h6>

                <button type="button"
                        class="btn-close"
                        data-bs-dismiss="modal"
                        aria-label="Close"
                        style="font-size: 12px;">

                </button>
            </div>
            <div class="modal-body pt-2">
                <form id="search-form">
                <div class="input-group">
                    <input type="text"
                           id="search-input"
                           @if($filters->search()) value="{{join(' ',$filters->search())}}" @endif
                           class="form-control"
                           placeholder="{{ trans('log_viewer.type_request') }}..."
                           aria-label="{{ trans('log_viewer.type_request') }}...">

                    <button class="btn m-0" type="submit">
                        <i class="bi bi-search"></i>
                    </button>
                </div>
                </form>
            </div>
        </div>
    </div>
</div>
