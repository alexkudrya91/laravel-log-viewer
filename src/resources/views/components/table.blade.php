@php
   $records = $attributes['records'];
   $is_inner = (bool) $attributes['is_inner'] ?? false;
@endphp
<table class="table table-bordered {{$is_inner ? ' w-100 m-0' : 'w-auto mx-3'}}">
    @foreach($records as $key => $value)
        <tr>
            @if (Arr::isAssoc($records)) <td>{{$key}}</td> @endif
            @if(is_array($value))
                    <td class="p-0"> <x-vendor.log_viewer.table :records="$value" :is_inner="true"/></td>
            @else
                <td>{{var_export($value, true)}}</td>
            @endif
        </tr>
    @endforeach
</table>
