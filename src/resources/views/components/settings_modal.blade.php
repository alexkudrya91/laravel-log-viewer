@php
    /** @var \AlexKudrya\LaravelLogViewer\Dto\FiltersDto $filters */
    $filters = $attributes['filters'];
@endphp
<div class="modal fade" id="settingsModal" tabindex="-1" aria-labelledby="settingsModalLabel" aria-modal="true">
<div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
        <div class="modal-header pb-0 px-4 border-bottom-0">
            <h6 class="modal-title fs-6" id="settingsModalLabel">{{ trans('log_viewer.menu.settings') }}...</h6>
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close" style="font-size: 12px;"></button>
        </div>
        <div class="modal-body pt-4">
            <form id="settings-form" method="POST" action="{{route('log_viewer.settings')}}">
                <div class="check-block">
                    <input class="form-check-input mt-0"
                           id="hide-same-dates"
                           type="checkbox"
                           name="hide_date"
                           @if($filters->isHideDates()) checked @endif
                           aria-label="Hide dates if they are the same">
                    <label for="hide-same-dates">{{ trans('log_viewer.settings.hide_dates') }}</label>
                </div>

                <div class="check-block mt-2">
                    <input class="form-check-input mt-0"
                           id="hide-same-environments"
                           type="checkbox"
                           name="hide_env"
                           @if($filters->isHideEnvironments()) checked @endif
                           aria-label="Hide environments if they are the same">
                    <label for="hide-same-environments">{{ trans('log_viewer.settings.hide_env') }}</label>
                </div>

                <div class="check-block mt-2">
                    <input class="form-check-input mt-0"
                           id="hide-same-levels"
                           type="checkbox"
                           name="hide_lvl"
                           @if($filters->isHideLevels()) checked @endif
                           aria-label="Hide levels if they are the same">
                    <label for="hide-same-levels">{{ trans('log_viewer.settings.hide_lvl') }}</label>
                </div>

                <br>

                <div class="check-block mt-2 lang-settings">
                    <select class="form-control mt-0"
                           id="locale"
                           name="locale"
                           aria-label="Hide levels if they are the same">
                        @foreach(config('log_viewer.locales') as $code => $locale)
                            <option value="{{ $code }}" @if($filters->locale() === $code) selected @endif>{{ $locale }}</option>
                        @endforeach
                    </select>
                    <label for="hide-same-levels">{{ trans('log_viewer.settings.lang') }}</label>
                </div>

                <div class="text-center mt-3">
                    <button class="btn" type="submit">{{ trans('log_viewer.btn.apply') }}</button>
                </div>
            </form>
        </div>
    </div>
</div>
</div>
