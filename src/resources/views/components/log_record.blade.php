@php
    /** @var \AlexKudrya\LaravelLogViewer\Dto\LogRecordDto $log_record */
   $log_record = $attributes['log_record']
@endphp
<div class="log-record level-{{ strtolower($log_record->level()) }}">
    <div class="collapse-top-btn" title="{{ trans('log_viewer.collapse') }}">
        <i class="fa-solid fa-chevron-up"></i>
    </div>
    @if ($log_record->date()) <div class="log-date" title="{{ trans('log_viewer.filters.date') }}: {{ $log_record->date() }}">{{ $log_record->date() }}</div>@endif
    @if ($log_record->time()) <div class="log-time" title="{{ trans('log_viewer.filters.time') }}: {{ $log_record->time() }}">{{ $log_record->time() }}</div>@endif
    @if ($log_record->environment()) <div class="log-env" title="{{ trans('log_viewer.filters.env') }}: {{ $log_record->environment() }}">{{ $log_record->environment() }}</div>@endif
    @if ($log_record->level()) <div class="log-level" title="{{ trans('log_viewer.filters.lvl') }}: {{ $log_record->level() }}">{{ $log_record->level() }}</div>@endif
    @if ($log_record->message()) <div class="log-message">{{ $log_record->message() }}</div>@endif
    @if ($log_record->rawContext()) <div class="log-raw-context">{{ $log_record->rawContext() }}</div>@endif
    @if ($log_record->stacktrace())
        <div class="log-stacktrace">
            <h6>{{ trans('log_viewer.stacktrace') }}:</h6>

            @foreach($log_record->stacktrace() as $stacktrace_row)
                <div class="stacktrace-row">{{ $stacktrace_row }}</div>
            @endforeach
        </div>
    @endif
    @if ($log_record->context())
        <div class="log-context">
            <h6>{{ trans('log_viewer.context') }}:</h6>
            <x-vendor.log_viewer.table :records="$log_record->context()"/>
        </div>
    @endif

    <div class="collapse-bottom-btn" title="{{ trans('log_viewer.collapse') }}">
        <i class="fa-solid fa-chevron-up"></i>
    </div>
</div>
