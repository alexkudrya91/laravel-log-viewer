<div class="d-flex flex-column flex-shrink-0 p-3 text-bg-dark log-viewer-sidebar">
    <div class="d-flex align-items-center mb-3 ms-2 mb-md-0 me-md-auto text-white hover-turquoise-text">
        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" fill="currentColor" class="bi bi-stack" viewBox="0 0 16 16">
            <path d="m14.12 10.163 1.715.858c.22.11.22.424 0 .534L8.267 15.34a.6.6 0 0 1-.534 0L.165 11.555a.299.299 0 0 1 0-.534l1.716-.858 5.317 2.659c.505.252 1.1.252 1.604 0l5.317-2.66zM7.733.063a.6.6 0 0 1 .534 0l7.568 3.784a.3.3 0 0 1 0 .535L8.267 8.165a.6.6 0 0 1-.534 0L.165 4.382a.299.299 0 0 1 0-.535z"/>
            <path d="m14.12 6.576 1.715.858c.22.11.22.424 0 .534l-7.568 3.784a.6.6 0 0 1-.534 0L.165 7.968a.299.299 0 0 1 0-.534l1.716-.858 5.317 2.659c.505.252 1.1.252 1.604 0z"/>
        </svg>
        <span class="ms-3 fs-4 sidebar-title">{{config('log_viewer.app_title') ?? 'Log Viewer'}}</span>
    </div>

    <hr>

    @if (!is_null($attributes['file']))
    <div>
        <div class="filters-btn sidebar-btn"
             id="filters-btn"
             title="{{ trans('log_viewer.menu.filters') }}">
            <i class="bi bi-funnel"></i> {{ trans('log_viewer.menu.filters') }}
        </div>
        <div class="search-btn sidebar-btn"
             id="search-btn"
             title="{{ trans('log_viewer.menu.search') }}">
            <i class="bi bi-search"></i> {{ trans('log_viewer.menu.search') }}
        </div>
        <a class="download-btn sidebar-btn mt-2 pe-2"
           title="{{ trans('log_viewer.menu.download') }}"
           id="download-btn"
           style="text-decoration: none; color: inherit"
           href="{{route('log_viewer.download', ['file' => $attributes['file']])}}"
           download>
            <i class="bi bi-cloud-download" style="font-size: 20px;"></i> {{ trans('log_viewer.menu.download') }}
        </a>
        <div class="settings-btn sidebar-btn mt-2 pe-2"
             id="settings-btn"
             title="{{ trans('log_viewer.menu.settings') }}">
            <i class="bi bi-gear" style="font-size: 20px;"></i> {{ trans('log_viewer.menu.settings') }}
        </div>
    </div>

    <hr>
    @endif

    <ul class="nav nav-pills links-list mb-auto">
        @php
            /** @var \AlexKudrya\LaravelLogViewer\Dto\LogFileDto $file */
        @endphp

        @foreach($attributes['files'] as $file)
            <li class="nav-item text-nowrap w-100 log-viewer-menu-item">
                <a href="{{route('log_viewer.index', ['page' => $file->name().'.'.$file->extension()])}}"
                   class="nav-link text-white mb-2 @if($attributes['file'] === ($file->name().'.'.$file->extension())) active @endif"
                   aria-current="page">
                    @if ($file->icon()) <i class="fa-{{$file->icon()->style()}} fa-{{$file->icon()->type()}}"></i>@endif
                    {{$file->name()}}.{{$file->extension()}}
                </a>
            </li>
        @endforeach
    </ul>

    <hr>

    <div class="nav-item text-nowrap">
        <button class="theme-switcher nav-link text-white py-2 px-4">
            <div class="switcher-btn switcher-auto">
                <i class="bi bi-circle-half me-3"></i> <span class="hover-text">{{ trans('log_viewer.theme.system') }}</span>
            </div>
            <div class="switcher-btn switcher-light" style="display: none">
                <i class="bi bi-sun-fill me-3"></i> <span class="hover-text">{{ trans('log_viewer.theme.light') }}</span>
            </div>
            <div class="switcher-btn switcher-dark" style="display: none">
                <i class="bi bi-moon-stars-fill me-3"></i> <span class="hover-text">{{ trans('log_viewer.theme.dark') }}</span>
            </div>
        </button>
    </div>
</div>
