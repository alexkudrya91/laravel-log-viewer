<!DOCTYPE html>
<html lang="{{ \Illuminate\Support\Facades\App::getLocale() }}">
<head>
    <title>
        @if(config('log_viewer.app_title'))
            {{ config('log_viewer.app_title') }}
        @else
            Log Viewer
        @endif
    </title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.10.5/font/bootstrap-icons.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-9ndCyUaIbzAi2FUVXJi0CjmCapSmO7SnpJef0486qhLnuZ2cdeRhO02iuK6FUUVM" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-geWF76RCwLtnZ8qwWowPQNguL3RmwHVBC9FhGdlKrxdiJJigb/j/68SIy3Te4Bkz" crossorigin="anonymous"></script>
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link rel="stylesheet" href="{{asset('css/log_viewer/styles.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/log_viewer/styles-dark.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/log_viewer/sidebars.min.css')}}">
    <link rel="icon" href="{{asset('favicon/log-viewer-favicon.ico')}}">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.7.1/jquery.min.js"></script>
    <script src="{{asset('js/log_viewer/theme-script.min.js')}}"></script>
    <script src="{{asset('js/log_viewer/script.min.js')}}"></script>
    <script src="{{asset('js/log_viewer/all.min.js')}}"></script>
    <script src="{{asset('js/log_viewer/jqh.js')}}"></script>
</head>
<body class="notranslate @if(($_COOKIE['log_viewer_dark_theme'] ?? 'false') === 'true') dark-mode @endif">

{{ $slot }}

<div class="bg"></div>

</body>
</html>
