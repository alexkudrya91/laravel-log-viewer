<?php

namespace AlexKudrya\LaravelLogViewer\Dto;

use AlexKudrya\LaravelLogViewer\Traits\MagicMethods;

/**
 * @method self|string style(?string $style = null)
 * @method self|string type(?string $type = null)
 */
class IconDto
{
    use MagicMethods;

    private string $style = 'solid';
    private string $type = 'box-archive';

    public function getStyle(): string
    {
        return $this->style;
    }

    public function setStyle(string $style): self
    {
        $this->style = $style;
        return $this;
    }

    public function getType(): string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        $this->type = $type;
        return $this;
    }
}