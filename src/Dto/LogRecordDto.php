<?php

namespace AlexKudrya\LaravelLogViewer\Dto;

use AlexKudrya\LaravelLogViewer\Traits\MagicMethods;

/**
 * @method self|string time(?string $time = null)
 * @method self|string date(?string $date = null)
 * @method self|string message(?string $message = null)
 * @method self|string level(?string $level = null)
 * @method self|string environment(?string $environment = null)
 * @method self|array stacktrace(?array $stacktrace = null)
 * @method self|array context(?array $context = null)
 * @method self|string rawMessage(?string $raw_message = null)
 * @method self|string rawStacktrace(?string $raw_stacktrace = null)
 * @method self|string rawContext(?string $raw_context = null)
 */
class LogRecordDto
{
    use MagicMethods;

    private ?string $date;
    private string $time;
    private string $message;
    private ?string $level;
    private ?string $environment;
    private array $stacktrace = [];
    private array $context = [];

    private string $raw_message;
    private ?string $raw_stacktrace;
    private ?string $raw_context;

    public function getTime(): string
    {
        return $this->time;
    }

    public function setTime(string $time): self
    {
        $this->time = $time;
        return $this;
    }

    public function getDate(): ?string
    {
        return $this->date;
    }

    public function setDate(?string $date): self
    {
        $this->date = $date;
        return $this;
    }

    public function getMessage(): string
    {
        return $this->message;
    }

    public function setMessage(string $message): self
    {
        $this->message = $message;
        return $this;
    }

    public function getLevel(): ?string
    {
        return $this->level;
    }

    public function setLevel(?string $level): self
    {
        $this->level = $level;
        return $this;
    }

    public function getEnvironment(): ?string
    {
        return $this->environment;
    }

    public function setEnvironment(?string $environment): self
    {
        $this->environment = $environment;
        return $this;
    }

    public function getStacktrace(): ?array
    {
        return $this->stacktrace;
    }

    public function setStacktrace(?array $stacktrace): self
    {
        $this->stacktrace = $stacktrace;
        return $this;
    }

    public function addStacktrace(string $key, string $value): self
    {
        $this->stacktrace[$key] = $value;
        return $this;
    }

    public function getContext(): array
    {
        return $this->context;
    }

    public function setContext(array $context): self
    {
        $this->context = $context;
        return $this;
    }

    public function addContext(string $key, string $value): self
    {
        $this->context[$key] = $value;
        return $this;
    }

    public function getRawMessage(): string
    {
        return $this->raw_message;
    }

    public function setRawMessage(string $raw_message): self
    {
        $this->raw_message = $raw_message;
        return $this;
    }

    public function getRawStacktrace(): ?string
    {
        return $this->raw_stacktrace;
    }

    public function setRawStacktrace(?string $raw_stacktrace): self
    {
        $this->raw_stacktrace = $raw_stacktrace;
        return $this;
    }

    public function getRawContext(): ?string
    {
        return $this->raw_context;
    }

    public function setRawContext(?string $raw_context): self
    {
        $this->raw_context = $raw_context;
        return $this;
    }
}
