<?php

namespace AlexKudrya\LaravelLogViewer\Dto;

use AlexKudrya\LaravelLogViewer\Traits\MagicMethods;

/**
 * @method self|array search(?array $search = null)
 * @method self|array levels(?array $levels = null)
 * @method self|array environments(?array $environments = null)
 * @method self|string date(?string $date = null)
 * @method self|string timeFrom(?string $timeFrom = null)
 * @method self|string timeTo(?string $timeTo = null)
 * @method self|string locale(?string $locale = null)
 */
class FiltersDto
{
    use MagicMethods;

    private ?array $search = null;
    private array $levels = [];
    private array $environments = [];
    private ?string $date = null;
    private ?string $timeFrom = null;
    private ?string $timeTo = null;
    private bool $hideDates = false;
    private bool $hideEnvironments = false;
    private bool $hideLevels = false;
    private string $locale = 'en';

    private bool $empty = true;

    public function getSearch(): ?array
    {
        return $this->search;
    }

    public function setSearch(?array $search): self
    {
        $this->search = $search;
        $this->empty = false;
        return $this;
    }

    public function getLevels(): array
    {
        return $this->levels;
    }

    public function setLevels(array $levels): self
    {
        $this->levels = $levels;
        $this->empty = false;
        return $this;
    }

    public function getEnvironments(): array
    {
        return $this->environments;
    }

    public function setEnvironments(array $environments): self
    {
        $this->environments = $environments;
        $this->empty = false;
        return $this;
    }

    public function getDate(): ?string
    {
        return $this->date;
    }

    public function setDate(?string $date): self
    {
        $this->date = $date;
        $this->empty = false;
        return $this;
    }

    public function getTimeFrom(): ?string
    {
        return $this->timeFrom;
    }

    public function setTimeFrom(?string $timeFrom): self
    {
        $this->timeFrom = $timeFrom;
        $this->empty = false;
        return $this;
    }

    public function getTimeTo(): ?string
    {
        return $this->timeTo;
    }

    public function setTimeTo(?string $timeTo): self
    {
        $this->timeTo = $timeTo;
        $this->empty = false;
        return $this;
    }

    public function isHideDates(): bool
    {
        return $this->hideDates;
    }

    public function hideDates(): self
    {
        $this->hideDates = true;
        return $this;
    }

    public function isHideEnvironments(): bool
    {
        return $this->hideEnvironments;
    }

    public function hideEnvironments(): self
    {
        $this->hideEnvironments = true;
        return $this;
    }

    public function isHideLevels(): bool
    {
        return $this->hideLevels;
    }

    public function hideLevels(): self
    {
        $this->hideLevels = true;
        return $this;
    }

    public function isEmpty(): bool
    {
        return $this->empty;
    }

    public function getLocale(): string
    {
        return $this->locale;
    }

    public function setLocale(string $locale): self
    {
        $this->locale = $locale;
        return $this;
    }
}
