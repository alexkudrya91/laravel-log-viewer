<?php

namespace AlexKudrya\LaravelLogViewer\Dto;

use AlexKudrya\LaravelLogViewer\Traits\MagicMethods;

/**
 * @method self|LogFileDto[] filesList(?array $files_list = null)
 * @method self|LogRecordDto[] logRecords(?array $log_records = null)
 * @method self|array dates(?array $dates = null)
 * @method self|array environments(?array $environments = null)
 * @method self|array levels(?array $levels = null)
 */
class LogViewDto
{
    use MagicMethods;

    /** @var $files_list LogFileDto[] */
    private array $files_list = [];

    /** @var $log_records LogRecordDto[] */
    private array $log_records = [];

    private array $dates = [];

    private array $environments = [];

    private array $levels = [];

    public function getFilesList(): array
    {
        return $this->files_list;
    }

    public function setFilesList(array $files_list): self
    {
        $this->files_list = $files_list;
        return $this;
    }

    public function addFile(LogFileDto $file): self
    {
        $this->files_list[] = $file;
        return $this;
    }

    public function getLogRecords(): array
    {
        return $this->log_records;
    }

    public function setLogRecords(array $log_records): self
    {
        $this->log_records = $log_records;
        return $this;
    }

    public function addRecord(LogRecordDto $record): self
    {
        $this->log_records[] = $record;
        return $this;
    }

    public function getDates(): array
    {
        return $this->dates;
    }

    public function setDates(array $dates): self
    {
        $this->dates = $dates;
        return $this;
    }

    public function addDate(string $date): self
    {
        if (!in_array($date, $this->dates)) {
            $this->dates[] = $date;
        }
        return $this;
    }

    public function sortDates(): self
    {
        asort($this->dates);
        $this->dates = array_values($this->dates);
        return $this;
    }

    public function isSingleDate(): bool
    {
        return count($this->dates) <= 1;
    }

    public function getMinDate(): ?string
    {
        return $this->dates[0] ?? null;
    }

    public function getMaxDate(): ?string
    {
        return $this->dates[count($this->dates) - 1] ?? null;
    }

    public function getEnvironments(): array
    {
        return $this->environments;
    }

    public function setEnvironments(array $environments): self
    {
        $this->environments = $environments;
        return $this;
    }

    public function addEnvironment(string $environment): self
    {
        if (!in_array($environment, $this->environments)) {
            $this->environments[] = $environment;
        }
        return $this;
    }

    public function sortEnvironments(): self
    {
        asort($this->environments);
        return $this;
    }

    public function isSingleEnvironment(): bool
    {
        return count($this->environments) <= 1;
    }

    public function getLevels(): array
    {
        return $this->levels;
    }

    public function setLevels(array $levels): self
    {
        $this->levels = $levels;
        return $this;
    }

    public function addLevel(string $level): self
    {
        if (!in_array($level, $this->levels)) {
            $this->levels[] = $level;
        }
        return $this;
    }

    public function sortLevels(): self
    {
        asort($this->levels);
        return $this;
    }

    public function isSingleLevel(): bool
    {
        return count($this->levels) <= 1;
    }
}
