<?php

namespace AlexKudrya\LaravelLogViewer\Dto;

use AlexKudrya\LaravelLogViewer\Traits\MagicMethods;

/**
 * @method self|string name(?string $name = null)
 * @method self|string extension(?string $extension = null)
 * @method self|IconDto icon(?IconDto $icon = null)
 */
class LogFileDto
{
    use MagicMethods;

    private string $name;
    private string $extension = 'log';
    private IconDto $icon;

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;
        return $this;
    }

    public function getExtension(): string
    {
        return $this->extension;
    }

    public function setExtension(string $extension): self
    {
        $this->extension = $extension;
        return $this;
    }

    public function getIcon(): IconDto
    {
        return $this->icon;
    }

    public function setIcon(IconDto $icon): self
    {
        $this->icon = $icon;
        return $this;
    }
}