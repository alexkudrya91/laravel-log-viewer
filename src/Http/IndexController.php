<?php

namespace AlexKudrya\LaravelLogViewer\Http;

use AlexKudrya\LaravelLogViewer\Services\LogViewGenerator;
use AlexKudrya\LaravelLogViewer\Helpers\FiltersHelper;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Log;
use Symfony\Component\HttpFoundation\BinaryFileResponse;

class IndexController
{
    public function __construct(
        private LogViewGenerator $generator,
        private FiltersHelper $helper,
    ) {}

    public function index(Request $request): View
    {
        $file_name = $request->get('page');
        $filters = Arr::except($request->all(), 'page');

        $settings = $this->helper->getSettings();

        App::setLocale($settings['locale'] ?? 'en');

        $this->helper->prepareFilters($filters, $settings);
        $filters_dto = $this->helper->filtersToDto($filters);
        $log_dto = $this->generator->generate($file_name, $filters_dto);

        return view('vendor.log_viewer.index', [
            'dto' => $log_dto,
            'filters' => $filters_dto,
            'file' => $file_name
        ]);
    }

    public function download(Request $request, string $file): Response|BinaryFileResponse
    {
        $file_path = $this->generator->getFile($file);

        if ($file_path) {
            Log::channel('log_viewer')->info('Downloading log file ' . $file, [
                'file_path' => $file_path,
                'IP' => request()->ip(),
            ]);

            return response()->download($file_path);
        }

        return response()->noContent();
    }

    public function saveSettings(Request $request): RedirectResponse
    {
        $hide_dates = $request->post('hide_date') === 'on';
        $hide_envs = $request->post('hide_env') === 'on';
        $hide_lvls = $request->post('hide_lvl') === 'on';
        $locale = $request->post('locale');

        return redirect()->back()->withCookies([
            cookie('hide_date', $hide_dates),
            cookie('hide_env', $hide_envs),
            cookie('hide_lvl', $hide_lvls),
            cookie('log_viewer_locale', $locale),
        ]);
    }
}
