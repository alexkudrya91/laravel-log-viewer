<?php

//-----------------------------------------------------------------
//
//                Laravel Log Viewer Configuration
//
//-----------------------------------------------------------------

return [

    //-------------------------------------------------------------
    // Route prefix for Log Viewer page,
    // By Default is "log-viewer" -> https://{your_host}/log-viewer
    //-------------------------------------------------------------

    'route_prefix' => 'log-viewer',

    //-------------------------------------------------------------
    // Title for your Log Viewer page,
    // located in <head/> -> <title/> tag
    //-------------------------------------------------------------

    'app_title' => 'Log Viewer',

    //-------------------------------------------------------------
    // Add middleware(s) for Log Viewer route,
    // for example 'auth' or some else middleware class
    //-------------------------------------------------------------

    'middleware' => \AlexKudrya\Adminix\Http\Middleware\AuthMiddleware::class,

    //-------------------------------------------------------------
    // Maximum number of records which can be displayed.
    // Some logs can contain hundreds of thousands of lines,
    // and it is too many to be displayed in one time.
    // Pagination for log doesn't make sense, so we need to
    // set this limit.
    //-------------------------------------------------------------

    'max_display_records' => env('LOG_VIEWER_MAX_LINES_DISPLAY',10000),

    //-------------------------------------------------------------
    // Locales list
    // To add new, create new translation file in /lang/{locale_code}/log_viewer.php,
    // then, add new line here -> {locale_code} => {locale_name}
    //-------------------------------------------------------------

    'locales' => [
        'en' => 'English',
        'ru' => 'Русский',
        'ua' => 'Українська'
    ]
];
