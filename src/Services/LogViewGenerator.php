<?php

namespace AlexKudrya\LaravelLogViewer\Services;

use AlexKudrya\LaravelLogViewer\Dto\FiltersDto;
use AlexKudrya\LaravelLogViewer\Dto\IconDto;
use AlexKudrya\LaravelLogViewer\Dto\LogFileDto;
use AlexKudrya\LaravelLogViewer\Dto\LogRecordDto;
use AlexKudrya\LaravelLogViewer\Dto\LogViewDto;
use Exception;
use Illuminate\Support\Facades\Log;
use phpDocumentor\Reflection\DocBlock\Tags\Factory\VarFactory;

class LogViewGenerator
{
    private LogViewDto $logView;
    private LogParserService $parserService;
    private array $icons;

    public function __construct()
    {
        $this->logView = new LogViewDto();
        $this->parserService = new LogParserService();
        $this->icons = require_once __DIR__.'/../icons_list.php';
    }

    public function generate(?string $file_name, FiltersDto $filters): LogViewDto
    {
        if (!is_null($file_name) && !$this->checkFileName($file_name)) {
            throw new Exception('Invalid log file name or file not in logs directory');
        }

        $this->getLogFilesList();

        if (!is_null($file_name)) {
            $this->getLogRecords($file_name, $filters);
        }

        return $this->logView;
    }

    public function getFile(string $file_name): ?string
    {
        if (!$this->checkFileName($file_name)) {
            return null;
        }

        $file_path = $this->getLogPath().'/'.$file_name;

        if (file_exists($file_path)) {
            return $file_path;
        }

        return null;
    }

    private function getLogRecords(string $file_name, FiltersDto $filters): void
    {
        $content = $this->getFileContent($file_name);

        if ($content) {
            $this->parseContent($content, $filters);
        }
    }

    private function getFileContent(string $file_name): null|string|false
    {
        $file_path = $this->getLogPath().'/'.$file_name;

        if (file_exists($file_path)) {
            return file_get_contents($file_path);
        }

        return null;
    }

    private function parseContent(string $content, FiltersDto $filters): void
    {
        $log_data = $this->parserService->parse($content);

        $i = 0;
        foreach ($log_data as $log) {
            $i++;
            $record = (new LogRecordDto())
                ->time($log['timestamp']['time'])
                ->date($log['timestamp']['date'])
                ->level($log['level'])
                ->environment($log['environment'])
                ->message($log['message'])
                ->stacktrace($log['stacktrace'] ?? [])
                ->context($log['context'] ?? [])
                ->rawMessage($log['raw_message'])
                ->rawStacktrace($log['raw_stacktrace'] ?? null)
                ->rawContext($log['raw_context'] ?? null);

            $this->logView->addDate($log['timestamp']['date'])
                ->addEnvironment($log['environment'])
                ->addLevel($log['level']);

            if (!$this->filter($record, $filters)) {
                continue;
            }

            $this->logView->addRecord($record);

            if ($i >= config('log_viewer.max_display_records', 10000)) break;
        }

        $this->logView->sortDates()->sortEnvironments()->sortLevels();
        $this->applyHiders($filters);
    }

    private function getLogFilesList(): void
    {
        $files = $this->getAllLogFiles();

        foreach ($files as $file) {
            if (str_contains($file, ".log")) {
                $file_name = explode(".", $file)[0];
                $file_extension = explode(".", $file)[1];

                $file_dto = (new LogFileDto())
                    ->name($file_name)
                    ->extension($file_extension);

                $this->findIcon($file_dto);

                $this->logView->addFile($file_dto);
            }
        }
    }

    private function getAllLogFiles(): ?array
    {
        $files = scandir($this->getLogPath());
        return array_diff($files, ['.', '..', '.gitignore']);
    }

    private function getLogPath(): string
    {
        return storage_path('logs');
    }

    private function findIcon(LogFileDto &$file_dto): void
    {
        $icon_dto = new IconDto();

        foreach ($this->icons as $icon_name => $icon) {
            if (str_contains($file_dto->name(), $icon_name)) {
                $icon_dto->style($icon[0])->type($icon[1]);
                break;
            }
        }

        $file_dto->icon($icon_dto);
    }

    private function filter(LogRecordDto $record, FiltersDto $filters): bool
    {
        if (!is_null($filters->search())) {
            $context_values = array_filter($record->context(), function($k) {
                return !is_bool($k);
            });

            foreach ($filters->search() as $search) {
                if (!in_array($search, $context_values)
                    && !str_contains($record->message(), $search)
                    && !str_contains($record->rawStacktrace(), $search)) {
                    return false;
                }
            }
        }

        if (!is_null($filters->timeFrom()) && $record->time() < $filters->timeFrom()) {
            return false;
        }

        if (!is_null($filters->timeTo()) && $record->time() > $filters->timeTo()) {
            return false;
        }

        if (!is_null($filters->date()) && $record->date() !== $filters->date()) {
            return false;
        }

        if (!empty($filters->levels()) && !in_array($record->level(), $filters->levels())) {
            return false;
        }

        if (!empty($filters->environments()) && !in_array($record->environment(), $filters->environments())) {
            return false;
        }

        return true;
    }

    private function applyHiders(FiltersDto $filters): void
    {
        foreach ($this->logView->logRecords() as &$logRecord) {
            if ($this->logView->isSingleDate() && $filters->isHideDates()) {
                $logRecord->date(null);
            }
            if ($this->logView->isSingleEnvironment() && $filters->isHideEnvironments()) {
                $logRecord->environment(null);
            }
            if ($this->logView->isSingleLevel() && $filters->isHideLevels()) {
                $logRecord->level(null);
            }
        }
    }

    private function checkFileName(string $file_name): bool
    {
        if (str_contains($file_name, "/") || !str_contains($file_name, ".log")) {
            Log::channel('log_viewer')->alert('Requested invalid log file name', [
                'file_name' => $file_name,
                'IP' => request()->ip(),
            ]);
            return false;
        }

        if (!in_array($file_name, $this->getAllLogFiles())) {
            Log::channel('log_viewer')->error('Requested log file not exists', [
                'file_name' => $file_name,
                'IP' => request()->ip(),
            ]);
            return false;
        }

        return true;
    }
}
