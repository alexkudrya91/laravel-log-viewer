<?php

namespace AlexKudrya\LaravelLogViewer\Services;

class LogParserService
{
    public function parse(string $content): array
    {
        $out = [];

        $timestamp_regex = '/(\\[\\d\\d\\d\\d-\\d\\d-\\d\\d \\d\\d:\\d\\d:\\d\\d\\])/im';
        $env_regex = '/([A-Za-z]+\\.(DEBUG|INFO|NOTICE|ALERT|WARNING|ERROR|EMERGENCY|CRITICAL):)/im';;
        $json_regex = '/({("[^"]*":"?[^"]*"?,?)+\})/im';

        $arr_1 = preg_split($timestamp_regex, $content, -1, PREG_SPLIT_NO_EMPTY | PREG_SPLIT_DELIM_CAPTURE);

        foreach ($arr_1 as $key => $value_1) {
            if (($key % 2) == 0) {
                $timestamp = trim($value_1, '\[\]');
                $out[]['timestamp'] = [
                    'date' => explode(' ', $timestamp)[0],
                    'time' => explode(' ', $timestamp)[1]
                ];
            } else {
                $out[count($out) - 1]['message'] = trim($value_1);
            }
        }

        foreach ($out as &$value) {
            $arr_2 = preg_split($env_regex, $value['message'], -1, PREG_SPLIT_NO_EMPTY | PREG_SPLIT_DELIM_CAPTURE);

            $value['message'] = trim($arr_2[2]);
            $value['level'] = $arr_2[1];
            $value['environment'] = explode('.', $arr_2[0])[0];
        }

        foreach ($out as &$value) {
            $value['raw_message'] = $value['message'];

            if (strripos($value['message'], '[stacktrace]') !== false) {
                $arr_3 = preg_split('/\\[stacktrace\\]/im', $value['message'], -1, PREG_SPLIT_NO_EMPTY);

                $value['message'] = trim($arr_3[0], " \n\r\t\v\0\"");
                $value['raw_stacktrace'] = $arr_3[1];
                $arr_4 = preg_split('/(#[0-9]+)/im', $arr_3[1], -1, PREG_SPLIT_NO_EMPTY);
                $value['stacktrace'] = [];

                foreach ($arr_4 as $stacktrace_item) {
                    if (preg_match('/[A-Za-z]+\\.[A-Za-z]+\\([0-9]+\\):/i', $stacktrace_item)) {
                        $value['stacktrace'][] = trim($stacktrace_item);
                    }
                }
            } elseif (strripos($value['message'], 'Stack trace:') !== false) {
                $arr_3 = preg_split('/Stack\\strace:/im', $value['message'], -1, PREG_SPLIT_NO_EMPTY);

                $value['message'] = trim($arr_3[0], " \n\r\t\v\0\"");
                $value['raw_stacktrace'] = trim($arr_3[1]);
                $arr_4 = preg_split('/(#[0-9]+)/im', $arr_3[1], -1, PREG_SPLIT_NO_EMPTY);

                $value['stacktrace'] = [];

                foreach ($arr_4 as $stacktrace_item) {
                    if (preg_match('/[A-Za-z]+\\.[A-Za-z]+\\([0-9]+\\):/i', $stacktrace_item)) {
                        $value['stacktrace'][] = trim($stacktrace_item);
                    }
                }
            } elseif (strripos($value['message'], 'Stacktrace:') !== false) {
                $arr_3 = preg_split('/Stacktrace:/im', $value['message'], -1, PREG_SPLIT_NO_EMPTY);

                $value['message'] = trim($arr_3[0], " \n\r\t\v\0\"");
                $value['raw_stacktrace'] = trim($arr_3[1]);
                $arr_4 = preg_split('/(#[0-9]+)/im', $arr_3[1], -1, PREG_SPLIT_NO_EMPTY);

                $value['stacktrace'] = [];

                foreach ($arr_4 as $stacktrace_item) {
                    if (preg_match('/[A-Za-z]+\\.[A-Za-z]+\\([0-9]+\\):/i', $stacktrace_item)) {
                        $value['stacktrace'][] = trim($stacktrace_item);
                    }
                }
            } elseif (preg_match($json_regex, $value['message'])) {
                $arr_5 = preg_split($json_regex, $value['message'], -1, PREG_SPLIT_NO_EMPTY | PREG_SPLIT_DELIM_CAPTURE);
                $value['message'] = trim($arr_5[0]);
                $value['raw_context'] = $arr_5[1];
                $value['context'] = json_decode($arr_5[1], true);
            }
        }

        return $out;
    }
}
